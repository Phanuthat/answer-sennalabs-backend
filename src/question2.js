var fs = require("fs");
const parse = require("csv-parse");

const sortListLastNameByAlphabet = (data = []) => {
  const sortNameByASC = data.sort((firstData, secondData) => {
    if (firstData && secondData && secondData.LastName && firstData.LastName) {
      if (firstData.LastName > secondData.LastName) {
        return 1;
      }
      if (firstData.LastName < secondData.LastName) {
        return -1;
      } else {
        return 0;
      }
    }
  });
  return sortNameByASC;
};

const readFile = () => {
  const parser = parse({
    columns: true,
  });

  let nameList = [];
  fs.createReadStream("./files/nameList.csv")
    .pipe(parser)
    .on("data", (data) => nameList.push(data))
    .on("end", () => {
      const afterSort = sortListLastNameByAlphabet(nameList);
      const display = afterSort.map(
        (name) => name.FirstName + " " + name.LastName
      );
      console.log(display)
    });
};
readFile();
